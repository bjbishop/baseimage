FROM debian:buster-20210408-slim

LABEL description="Base Debian 10.5-slim image with user 'app' and workdir /app"
LABEL git="git@gitlab.com:bjbishop/baseimage.git"
LABEL repo="registry.gitlab.com/bjbishop/baseimage"

# Downstream containers from this will clean up their cache
# hadolint ignore=DL3009
ONBUILD RUN apt-get --quiet update && \
	apt-get --quiet install --no-install-recommends --yes \
	openssh-client=1:7.9p1-10+deb10u2 \
	git=1:2.20.1-2+deb10u3 \
	curl=7.64.0-4+deb10u1 \
	ca-certificates=20200601~deb10u1

ONBUILD RUN addgroup --system app && \
	adduser --quiet --home /app --shell /bin/sh --system app

ONBUILD WORKDIR /app

ONBUILD USER app

CMD ["/bin/bash"]